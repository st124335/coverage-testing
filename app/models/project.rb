class Project < ApplicationRecord
    has_many :students
    validates_presence_of :name, :url
    validates :name, presence: true, format: { without: /[.?#!*]/, message: "contains invalid characters" }
    validates :url, presence: true, format: { without: /[?!*]/, message: "contains invalid characters" }
end
