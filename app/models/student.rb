class Student < ApplicationRecord
  belongs_to :project
  validates :name, presence: true, format: { without: /[.?#!*]/, message: "contains invalid characters" }
  validates :studentid, presence: true, format: { without: /[?!*]/, message: "contains invalid characters" }
end
