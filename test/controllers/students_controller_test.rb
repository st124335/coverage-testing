require "test_helper"

class StudentsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @student = students(:one)
  end

  test "should get index" do
    get students_url
    assert_response :success
  end

  test "should get new" do
    get new_student_url
    assert_response :success
  end

  test "should create student" do
    assert_difference("Student.count") do
      post students_url, params: { student: { name: @student.name, project_id: @student.project_id, studentid: @student.studentid } }
    end

    assert_redirected_to student_url(Student.last)
  end

  test "should faild to create student due to the absence of username/url" do
    assert_no_difference("Student.count") do
      post students_url, params: { student: { junk:'XYZ'} }
    end

    # assert_redirected_to project_url(Project.last)
  end

  test "should show student" do
    get student_url(@student)
    assert_response :success
  end

  test "should get edit" do
    get edit_student_url(@student)
    assert_response :success
  end

  test "should update student" do
    patch student_url(@student), params: { student: { name: @student.name, project_id: @student.project_id, studentid: @student.studentid } }
    assert_redirected_to student_url(@student)
  end

  test "should not update student with invalid data" do
    patch student_url(@student), params: { student: { name: "Updated Name?!?", project_id: @student.project_id, studentid: "invalid id" } }
    assert_response :unprocessable_entity
    
    @student.reload
    assert_not_equal "Updated Name?!?", @student.name
    # Add more assertions for other attributes if needed
  end

  test "should destroy student" do
    assert_difference("Student.count", -1) do
      delete student_url(@student)
    end

    assert_redirected_to students_url
  end
  
end
