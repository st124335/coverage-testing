require "test_helper"

class ProjectsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @project = projects(:one)
  end

  test "should get index" do
    get projects_url
    assert_response :success
  end

  test "should get new" do
    get new_project_url
    assert_response :success
  end

  test "should create project" do
    assert_difference("Project.count") do
      post projects_url, params: { project: { name: @project.name, url: @project.url } }
    end

    assert_redirected_to project_url(Project.last)
  end

  test "should faild to create project due to the absence of username/url" do
    assert_no_difference("Project.count") do
      post projects_url, params: { project: { junk:'XYZ'} }
    end
    assert_select "li", "Name can't be blank"
    assert_select "li", "Url can't be blank"
    # assert_redirected_to project_url(Project.last)
  end

  test "should show project" do
    get project_url(@project)
    assert_response :success
  end

  test "should get edit" do
    get edit_project_url(@project)
    assert_response :success
  end

  test "should update project" do
    patch project_url(@project), params: { project: { name: @project.name, url: @project.url } }
    assert_redirected_to project_url(@project)
  end

  # test "should failed to update project" do
  #   project = projects(:one)
  
  #   patch project_url(@project), params: { project: { name: @project.name, url: @project.url } }
  
  #   # assert_redirected_to project_path(project)
  #   # Reload association to fetch updated data and assert that title is updated.
  #   project.reload
  #   assert_not_equal "updated", project.name
  #   assert_not_equal "updated", project.url
  # end

  test "should not update project with invalid name" do
    project = projects(:one)  # Assuming you have a fixture or factory for a project

    patch project_url(project), params: { project: { name: "Project?!Name", url: "http://example.com" } }

    assert_response :unprocessable_entity
    assert_match /contains invalid characters/, response.body
    project.reload  # Reload the project to get the latest data
    assert_not_equal "Project?!Name", project.name
  end

  test "should not update project with invalid url" do
    project = projects(:one)  # Assuming you have a fixture or factory for a project

    patch project_url(project), params: { project: { name: "Valid Project Name", url: "http://example.com?" } }

    assert_response :unprocessable_entity
    assert_match /contains invalid characters/, response.body
    project.reload  # Reload the project to get the latest data
    assert_not_equal "http://example.com?", project.url
  end
  

  test "should destroy project" do
    assert_difference("Project.count", -1) do
      @project.students.each do |s|
        s.delete
      end
      delete project_url(@project)
    end

    assert_redirected_to projects_url
  end
end
